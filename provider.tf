
terraform {
	backend "s3" {
		bucket = "cf-templates-1bdxdp30rbh5h-us-west-2"
		key    = "foo/terraform.tfstate"
		region = "us-west-2"
	}
}
terraform {
  required_providers {
    aws = {
      version = "~> 3.0"
      source = "hashicorp/aws"
    }
  }
}

provider "aws" {
  region     = var.ec2_region
}
