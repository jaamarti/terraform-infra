# resource "aws_instance" "OneServer" {
#   ami           = var.ec2_image
#   instance_type = var.ec2_instance_type
# ####  key_name      = var.ec2_keypair
#   count         = var.ec2_count
#   tags = {
#     Name = var.ec2_tags
#   }
# }

# output "instance_ip_addr" {
#   value       = aws_instance.OneServer.*.private_ip
#   description = "The private IP address of the main server instance."
# }

# output "instance_ips" {
#   value = aws_instance.OneServer.*.public_ip
# }

data "aws_ec2_managed_prefix_list" "list" {
  name   = "commsearch"
}

resource "aws_security_group" "allow_tls" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = "vpc-84ed4cfd"

  ingress {
    description      = "TLS from VPC"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    prefix_list_ids      = [data.aws_ec2_managed_prefix_list.list.id]
  }

  tags = {
    Name = "allow_tls"
  }
}

# resource "aws_ec2_managed_prefix_list" "commsearch_list" {
#   name           = "commsearch"
#   address_family = "IPv4"
#   max_entries    = 5

#   entry {
#     cidr        = "10.0.0.0/16"
#     description = "Primary"
#   }

#   entry {
#     cidr        = "172.2.0.0/16"
#     description = "Secondary"
#   }

#   tags = {
#     Env = "live"
#   }
# }
